//
//  ViewController.swift
//  TableCat
//
//  Created by Павел Зорин on 16.04.2020.
//  Copyright © 2020 Zorin Media. All rights reserved.
//

import UIKit

class TableCatViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    private let cellId: String = "catCell"
    private var model: [SearchModel] = []
    private var json: [[String: Any]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        self.connect(){ error, message, modelData, jsonData in
            if error {
                self.dialogAlert(message: message)
                return
            }
            self.model = modelData
            self.json = jsonData
            self.tableView.reloadData()
        }
    }
    // MARK: Connect
    private func connect(completion: @escaping (_ error: Bool, _ message: String, _ modelData: [SearchModel], _ jsonData: [[String: Any]]) -> Void) {
        var errorState: Bool = false
        var message: String = ""
        var modelData: [SearchModel] = []
        var jsonData: [[String: Any]] = []
        var request = URLRequest(url: URL(string: "https://api.thecatapi.com/v1/images/search?limit=\(Int.random(in: 20...50))&page=\(Int.random(in: 1...100))")!)
        request.httpMethod = "GET"
        let session = URLSession.shared
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let httpResponse = response as? HTTPURLResponse {
                let code = httpResponse.statusCode
                switch code {
                case 200...201:
                    guard let data = data else {
                        completion(true, "Data error", modelData, jsonData)
                        return
                    }
                    do {
                        modelData =  try JSONDecoder().decode([SearchModel].self, from: data)
                        jsonData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [[String: Any]]
                    } catch let modelErr {
                        errorState = true
                        message = modelErr.localizedDescription
                    }
                default:
                    errorState = true
                    message = error?.localizedDescription ?? "Undefined error"
                }
            }
            DispatchQueue.main.async {
                completion(errorState, message, modelData, jsonData)
            }
        }).resume()
    }
    // MARK: Dialog
    private func dialogAlert(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    @IBAction func unwindToViewController (sender: UIStoryboardSegue){}
}
// MARK: Table DataSource
extension TableCatViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! CatTableViewCell
        cell.configure(with: model[indexPath.row])
        cell.tag = indexPath.row
        print(cell.cellImage.frame)
        return cell
    }
}
// MARK: Table Delegate
extension TableCatViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let selectedCell = tableView.cellForRow(at: indexPath) as? CatTableViewCell,
            let image = selectedCell.cellImage.image
            else {
                return
        }
        if let mvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ImageCat") as? ImageCatViewController {
            mvc.image = image
            mvc.text = String(data: try! JSONSerialization.data(withJSONObject: self.json[indexPath.row], options: .prettyPrinted), encoding: .utf8)!
            self.present(mvc, animated: true, completion: nil)
        }
    }
}
