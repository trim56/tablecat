//
//  ImageViewController.swift
//  TableCat
//
//  Created by Павел Зорин on 16.04.2020.
//  Copyright © 2020 Zorin Media. All rights reserved.
//

import UIKit

class ImageCatViewController: UIViewController {
    
    @IBOutlet weak var imageCat: UIImageView!
    @IBOutlet weak var jsonCat: UITextView!
    
    var image: UIImage!
    var text: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.imageCat.image = self.image
            self.jsonCat.text = self.text
        }
    }    
}
