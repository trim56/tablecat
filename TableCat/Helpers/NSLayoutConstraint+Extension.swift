//
//  NSLayoutConstraint+Extension.swift
//  TableCat
//
//  Created by Павел Зорин on 16.04.2020.
//  Copyright © 2020 Zorin Media. All rights reserved.
//

import UIKit

extension NSLayoutConstraint {

    func changeMultiplier(multiplier: CGFloat) -> NSLayoutConstraint {
      let newConstraint = NSLayoutConstraint(
        item: firstItem!,
        attribute: firstAttribute,
        relatedBy: relation,
        toItem: secondItem,
        attribute: secondAttribute,
        multiplier: multiplier,
        constant: constant)
      newConstraint.priority = priority
        NSLayoutConstraint.deactivate([self])
        NSLayoutConstraint.activate([newConstraint])
      return newConstraint
    }
}
