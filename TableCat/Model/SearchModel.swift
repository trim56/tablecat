//
//  SearchModel.swift
//  TableCat
//
//  Created by Павел Зорин on 16.04.2020.
//  Copyright © 2020 Zorin Media. All rights reserved.
//

struct SearchModel: Decodable {
  var id: String
  var url: String
  var width: Int
  var height: Int
}


