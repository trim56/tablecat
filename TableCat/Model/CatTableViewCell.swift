//
//  CatTableViewCell.swift
//  TableCat
//
//  Created by Павел Зорин on 16.04.2020.
//  Copyright © 2020 Zorin Media. All rights reserved.
//

import UIKit
import Combine

class CatTableViewCell: UITableViewCell {

    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var aspectRatioImage: NSLayoutConstraint!
    @IBOutlet weak var indicatorLoad: UIActivityIndicatorView!
    private var cancellable: AnyCancellable?
//    private var animator: UIViewPropertyAnimator?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    override public func prepareForReuse() {
        super.prepareForReuse()
        self.indicatorLoad.isHidden = false
        self.indicatorLoad.startAnimating()
        
        cellImage.image = nil
//        cellImage.alpha = 0.0
//        animator?.stopAnimation(true)
        cancellable?.cancel()
    }
    public func configure(with search: SearchModel) {
        cellLabel.text = "\(search.width)x\(search.height)"
        if search.width != 0 {
            aspectRatioImage = aspectRatioImage.changeMultiplier(multiplier: CGFloat(search.height) / CGFloat(search.width))
        }
        cancellable = loadImage(for: search).sink { [unowned self] image in self.showImage(image: image) }
    }
    private func showImage(image: UIImage?) {
//        cellImage.alpha = 0.0
//        animator?.stopAnimation(false)
        cellImage.image = image
        self.indicatorLoad.isHidden = true
//        animator = UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
//            self.cellImage.alpha = 1.0
//        })
    }
    private func loadImage(for search: SearchModel) -> AnyPublisher<UIImage?, Never> {
        return Just(search.url)
        .flatMap({ url -> AnyPublisher<UIImage?, Never> in
            let url = URL(string: search.url)!
            return ImageLoader.shared.loadImage(from: url)
        })
        .eraseToAnyPublisher()
    }
}
